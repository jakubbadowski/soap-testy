<?php

class Library
{
    public function __construct()
    {
        // Sprawdzamy zmienne globalne
        if ($_SERVER['PHP_AUTH_USER'] === "kuba" && $_SERVER['PHP_AUTH_PW'] === "123") {
            return true;
        }
        throw new SoapFault("Wrong auth", 401);
    }

    public function greetUser($name) 
    {
        return array("message" => "Cześć, " . $name);
    }

    public function getOrdersStatuses()
    {
    	return [
    		"id" => 1,
    		"name" => "Oczekiwanie na płatność czekiem",
    		"canceled" => 0
    	];
    }
}

// Zapis requestu do pliku
$header = print_r(getallheaders(), true);
$body = file_get_contents('php://input');
$logData = sprintf("%s\nHeader: %sBody: %s\n", date('Y-m-d H:i:s'), $header, $body );
file_put_contents('request.log', $logData, FILE_APPEND);

$options = array("uri" => "http://localhost" );
$server = new SoapServer(null, $options);
$server->setClass('Library');
$server->handle();