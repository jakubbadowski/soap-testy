<?php
// Uwierzytelnienie Basic Auth
$options = array("location" => "http://localhost/soap-testy/uwierzytelnienie_ba/server.php", 
					"uri" => "urn://localhost/soap-testy/uwierzytelnienie_ba/server.php",
                    "login" => "kuba", // ustawiamy login
                    "password" => "1233", // ustawiamy hasło
					"trace" => 1 ); // Tryb debug
try {

    $client = new SoapClient(null, $options);

    $greet = $client->greetUser("Kuba");
    var_dump($greet);

} catch (SoapFault $e) {
    print_r($e);
}