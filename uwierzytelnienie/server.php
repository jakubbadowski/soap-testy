<?php

class Library
{
    public function authenticate($header_params)
    {
        if ($header_params->user === "kuba" && $header_params->pass === "123") {
            return true;
        } else {
            
            throw new SoapFault('Wrong login/pass', 401);
        }
    }

    public function greetUser($name) 
    {
        return array("message" => "Cześć, " . $name);
    }

    public function getOrdersStatuses()
    {
    	return [
    		"id" => 1,
    		"name" => "Oczekiwanie na płatność czekiem",
    		"canceled" => 0
    	];
    }
}

$options = array("uri" => "http://localhost" );
$server = new SoapServer(null, $options);
$server->setClass('Library' );
$server->handle();