<?php

$options = array("location" => "http://localhost/soap-testy/uwierzytelnienie/server.php", 
					"uri" => "urn://localhost/soap-testy/uwierzytelnienie/server.php",
					"trace" => 1 ); // Tryb debug
try {

    $client = new SoapClient(null, $options);

    // Uwierzytelnienie
    $auth = new stdClass();
    $auth->user = "kuba";
    $auth->pass = "123";

    $header_params = new SoapVar($auth, SOAP_ENC_OBJECT);
    $header = new SoapHeader(
    					'cameleon',  // prestrzeń nazw - dowolna nazwa!
    					'authenticate', // nazwa funkcji ipodwiedzialnej za uwierzytelnienie
    					$header_params,
    					false
    				);
    $client->__setSoapHeaders([$header]); // parametr musi być tablicą

    $greet = $client->greetUser("Kuba");
    var_dump($greet);

} catch (SoapFault $e) {
    print_r($e);
}