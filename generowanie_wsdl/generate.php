<?php

require __DIR__ . '/../vendor/autoload.php';
require 'Library.php';

$class = "Library";
$serviceURI = "http://localhost/soap-testy/generowanie_wsdl/server.php";
$wsdlGenerator = new PHP2WSDL\PHPClass2WSDL($class, $serviceURI);
// Generate the WSDL from the class adding only the public methods that have @soap annotation.
$wsdlGenerator->generateWSDL();
// $wsdlXML = $wsdlGenerator->dump();
// print_r($wsdlXML);

// Or save as file
$wsdlXML = $wsdlGenerator->save('example.xml');