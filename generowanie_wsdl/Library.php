<?php

class Library
{
    /**
      * @var string $name Imię
      * @return string Wyświetla powitanie
    */
    public function greetUser(string $name)
    {
        return array("message" => "Cześć, " . $name);
    }

    /**
      * @return string Lista dostępnych satusów
    */
    public function getOrdersStatuses()
    {
        // return false;
        $zamowienia = ['order 1', 'order 2', 'order3'];

        $output = new \StdClass; // Zwracamy tablicę obiektów

        $output = new stdClass();
        $output->orders = new ArrayObject();
        
        // Tu iterujemy po wynikach z bazy danych
        foreach($zamowienia as $item) {

            // Tu tworzymy pojedynczy order
            $order = new StdClass;
            $order->id = 10;
            $order->name = $item;
            // i tak dalej

            // Klient
            $client = new StdClass;
            $client->userId = 123;
            $client->email = "kuba@ex.pl <test></test>";
            $soapVar = new SoapVar($client,SOAP_ENC_OBJECT,NULL,NULL,'client');
            $order->client = $soapVar;

            // Produkty z bazy
            $prodFromDb = [ 1, 2 ];
            $order->products = new ArrayObject();

            foreach ($prodFromDb as $p) {
                // Products
                $product = new StdClass;
                $product->counter = $p;
                $product->code = "XYZ";
                $soapVar = new SoapVar($product,SOAP_ENC_OBJECT,NULL,NULL,'product');
                $order->products->append($soapVar);
            }

            $soapVar = new SoapVar($order,SOAP_ENC_OBJECT,NULL,NULL,'order');
            $output->orders->append($soapVar);
        }
        return $output;   
    }
}
