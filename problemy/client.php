<?php

$options = array("location" => "http://localhost/soap-testy/problemy/server.php", 
					"uri" => "urn://cokolwiek",
					"trace" => true ); // Tryb debug
try {

    $client = new SoapClient(null, $options);

    $client->getOrdersStatuses();

    var_dump( $client->__getLastRequestHeaders() );
    echo PHP_EOL;
    var_dump( $client->__getLastRequest() );
    echo PHP_EOL;


    var_dump( $client->__getLastResponseHeaders() );
    echo PHP_EOL;
    var_dump( $client->__getLastResponse() );
    echo PHP_EOL;

} catch (SoapFault $e) {
    print_r($e);
}