<?php

class Library
{
    public function greetUser($name) 
    {
        return array("message" => "Cześć, " . $name);
    }

    public function getOrdersStatuses()
    {
    	return [
    		"id" => 1,
    		"name" => "Oczekiwanie na płatność czekiem",
    		"canceled" => 0
    	];
    }
}

$options = array("uri" => "http://localhost" );
$server = new SoapServer(null, $options);
$server->setClass('Library' );
$server->handle();