<?php

$options = array("location" => "http://localhost/soap-testy/prosty_przyklad/server.php", 
					"uri" => "urn://localhost/soap-testy/prosty_przyklad/server.php",
					"trace" => 1 ); // Tryb debug
try {

    $client = new SoapClient(
    						null, // NULL w pierwszym parametrze oznacza tryb bez WSDL
    						$options // ponieważ brak WSDL, trzeba podać podstawowe opcje w drugim prametrze konstruktora. W trybie z WSDL konstruktor klasy SoapClient ma jeden parametr, wskazujący na URL dokumentu WSDL, a w nim już się znajdują wszystkie potrebne informacje
    					);  
    $greet = $client->greetUser("Kuba");
    var_dump($greet);

    $statuses = $client->__soapCall('getOrdersStatuses', []); // Inna opcja wołania metody, drugi argument to lista parametrów wołanej metody
    print_r($statuses);

} catch (SoapFault $e) {
    var_dump($e);
}